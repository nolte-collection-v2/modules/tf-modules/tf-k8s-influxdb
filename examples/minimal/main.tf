resource "kubernetes_namespace" "influxdb" {
  metadata {
    name = "influxdb"
  }
}


module "install" {
  source    = "./../../modules/install"
  namespace = kubernetes_namespace.influxdb.metadata[0].name
}


output "helm_release" {
  value = module.install.release
}
