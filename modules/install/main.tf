
locals {
  EXTRA_VALUES = {
  }
}


resource "helm_release" "release" {
  name       = "influxdb"
  repository = "https://helm.influxdata.com/"
  chart      = "influxdb"
  version    = var.chart_version
  namespace  = var.namespace
  values = [
    yamlencode(local.EXTRA_VALUES),
    yamlencode(var.extra_values)
  ]
}
