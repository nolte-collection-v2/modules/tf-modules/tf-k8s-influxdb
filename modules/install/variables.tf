variable "namespace" {
}

variable "chart_version" {
  default = "4.8.5"
}

variable "extra_values" {
  default = {}
}
