module github.com/nolte/tf-k8s-influxdb

go 1.14

require (
	github.com/gruntwork-io/terratest v0.30.3
	github.com/stretchr/testify v1.6.1
)
